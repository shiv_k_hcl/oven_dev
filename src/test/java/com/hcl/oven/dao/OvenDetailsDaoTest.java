package com.hcl.oven.dao;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import java.util.Optional;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import com.hcl.oven.entity.OvenDetails;
import com.hcl.oven.repository.OvenDetailsRepository;

@RunWith(SpringRunner.class)
@Transactional
@SpringBootTest
public class OvenDetailsDaoTest {

	@Autowired
	private OvenDetailsRepository ovenDetailsRepository;

	@Test
	public void getOvenDetailsByIdOutputTestPositive() {

		if (ovenDetailsRepository.getOvenDetailsById(1L).isPresent()) {
			Optional<OvenDetails> actualList = Optional.ofNullable(ovenDetailsRepository.getOvenDetailsById(1L).get());
			assertThat(actualList).isNotNull();
		}
	}

	@Test
	public void getOvenDetailsByIdOutputTestNegative() {

		if (ovenDetailsRepository.getOvenDetailsById(5L).isEmpty()) {
			Optional<Optional<OvenDetails>> actualList = Optional
					.ofNullable(ovenDetailsRepository.getOvenDetailsById(5L));
			assertThat(actualList).isPresent();
		}
	}

	@Test
	public void getOvenCountOutputTestNegative() {

		if (!ovenDetailsRepository.findAll().isEmpty()) {
			Optional<Integer> size = Optional.ofNullable(ovenDetailsRepository.findAll().size());
			assertNotEquals(8, size);
		}
	}

}
