package com.hcl.oven.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import com.hcl.oven.entity.OvenDetails;
import com.hcl.oven.exception.OvenDetailsNotFoundException;
import com.hcl.oven.repository.OvenDetailsRepository;
import com.hcl.oven.rest.dto.OvenDto;
import com.hcl.oven.validation.OvenDetailsValidation;

@RunWith(SpringRunner.class)
@WebMvcTest(value = OvenDetailsServiceImpl.class)
public class OvenDetailsServiceImplTest {

	@InjectMocks
	private OvenDetailsServiceImpl ovenService;

	@MockBean
	private OvenDetailsRepository ovenRepository;

	@MockBean
	private OvenDetailsValidation ovenDetailsValidation;

	List<OvenDto> oventest = new ArrayList<OvenDto>();
	List<OvenDetails> ovenDetailsList = new ArrayList<OvenDetails>();
	Optional<OvenDetails> ovenDetail = Optional.ofNullable(new OvenDetails());
	OvenDto oven = new OvenDto();

	@Before
	public void setUp() {

		MockitoAnnotations.initMocks(this);
		MockMvcBuilders.standaloneSetup(ovenService).build();

		oven.setBrand("samsung");
		oven.setName("SM12");
		oven.setId("1");
		oven.setPower("500W");
		oventest.add(oven);

		ovenDetail.get().setId(1L);
		ovenDetail.get().setBrandName("samsung");
		ovenDetail.get().setOvenName("SM12");
		ovenDetail.get().setPower("300W");
		ovenDetail.get().setTemp(80);
		ovenDetail.get().setTimer(30);
		ovenDetailsList.add(ovenDetail.get());

	}

	@Test
	public void getById_shouldReturnResponseStatusOK() throws OvenDetailsNotFoundException {

		Mockito.when(ovenRepository.getOvenDetailsById(1L)).thenReturn(ovenDetail);
		Optional<OvenDto> ovenItem = ovenService.getById(1L);
		ovenItem.ifPresent(value -> assertEquals(ovenDetail.get().getBrandName(), value.getBrand()));
	}

	@Test
	public void getAll_shouldReturnResponseStatusOK() throws OvenDetailsNotFoundException {

		Mockito.when(ovenRepository.findAll()).thenReturn(ovenDetailsList);
		List<OvenDto> ovenItemList = ovenService.getAll();
		assertNotNull(ovenItemList);
	}

	@Test
	public void getById_shouldReturnResponseNotMatch() throws OvenDetailsNotFoundException {

		Mockito.when(ovenRepository.getOvenDetailsById(1L)).thenReturn(ovenDetail);
		Optional<OvenDto> ovenItem = ovenService.getById(1L);
		ovenItem.ifPresent(value -> assertNotEquals(ovenDetail.get().getBrandName(), "SANSUI"));
	}

	@After
	public void tearDown() {

		oventest.clear();
		ovenDetailsList.clear();

	}

}
