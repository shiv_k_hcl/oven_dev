package com.hcl.oven.rest.controller;

import static org.junit.Assert.assertEquals;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import com.hcl.oven.rest.dto.OvenDto;
import com.hcl.oven.service.OvenDetailsServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class OvenApplicationControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Mock
	OvenDetailsServiceImpl ovenDetailsServiceImpl;

	@InjectMocks
	OvenApplicationController ovenApplicationController;

	private String ovenList;
	List<OvenDto> oventest;
	OvenDto oven;

	@Before
	public void setUp() {

		oventest = new ArrayList<OvenDto>();
		oven = new OvenDto();
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(ovenApplicationController).build();

		oven.setBrand("samsung");
		oven.setName("SM12");
		oven.setId("1");
		oven.setPower("500W");
		oventest.add(oven);

		ovenList = "[{\"brand\":\"SAMSUNG\",\"id\":\"1\",\"name\":\"SM12\",\"power\":\"500W\",\"status\":\"ON\",\"temparature\":0,\"time\":0}]";
	}

	@Test
	public void getAllOven_shouldReturnResponseStatusPositive() throws Exception {

		Mockito.when(ovenDetailsServiceImpl.getAll()).thenReturn(oventest);
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/ovenApp/getAllOven")
				.accept(MediaType.APPLICATION_JSON).content(ovenList).contentType(MediaType.APPLICATION_JSON);
		MvcResult result = this.mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();

		assertEquals(HttpStatus.OK.value(), response.getStatus());
	}

	@Test
	public void getAllOven_shouldReturnResponseStatusNegative() throws Exception {

		Mockito.when(ovenDetailsServiceImpl.getAll()).thenReturn(null);
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/ovenApp/getAllOven")
				.accept(MediaType.APPLICATION_JSON).content(ovenList).contentType(MediaType.APPLICATION_JSON);
		MvcResult result = this.mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();

		assertEquals(HttpStatus.OK.value(), response.getStatus());
	}

	@Test
	public void getOvenById_shouldReturnResponseStatusOK() throws Exception {

		Mockito.when(ovenDetailsServiceImpl.getById(1L)).thenReturn(java.util.Optional.ofNullable(oven));
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/ovenApp/getOvenDetails/1")
				.accept(MediaType.APPLICATION_JSON).content(ovenList).contentType(MediaType.APPLICATION_JSON);
		MvcResult result = this.mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();

		assertEquals(HttpStatus.OK.value(), response.getStatus());
	}

	@Test
	public void addOven_shouldReturnResponseStatusNotFound() throws Exception {

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/ovenApp/addNewOven")
				.accept(MediaType.APPLICATION_JSON).content(ovenList).contentType(MediaType.APPLICATION_JSON);
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();

		assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatus());
	}

	@Test
	public void updateOvenDetails_shouldReturnResponseStatusOK() {

		Mockito.when(ovenDetailsServiceImpl.updateOven("1", oven)).thenReturn(true);
		ovenDetailsServiceImpl.updateOven("1", oven);
		Mockito.verify(ovenDetailsServiceImpl).updateOven("1", oven);

	}

	@Test
	public void updateOvenStatus_shouldReturnResponseStatusOK() throws Exception {

		Mockito.when(ovenDetailsServiceImpl.updateOvenStatus("100", "23", "1")).thenReturn("UPDATED");
		ovenDetailsServiceImpl.updateOvenStatus("100", "23", "1");
		Mockito.verify(ovenDetailsServiceImpl).updateOvenStatus("100", "23", "1");

	}

	@Test
	public void deletOvenDetails_shouldReturnResponseStatusOK() throws Exception {

		Mockito.doNothing().when(ovenDetailsServiceImpl).delete("1");
		RequestBuilder requestBuilder = MockMvcRequestBuilders.delete("/ovenApp/deleteOvenDetails/1")
				.accept(MediaType.APPLICATION_JSON).content(ovenList).contentType(MediaType.APPLICATION_JSON);
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();

		assertEquals(HttpStatus.OK.value(), response.getStatus());
	}

	@Test
	public void updateOvenOnOffStatus_shouldReturnResponseStatusOK() throws Exception {

		Mockito.doNothing().when(ovenDetailsServiceImpl).updateOvenOnOffStatus("1");
		ovenDetailsServiceImpl.updateOvenOnOffStatus("1");
		Mockito.verify(ovenDetailsServiceImpl).updateOvenOnOffStatus("1");

	}

	@Test
	public void getAllOven_shouldReturnResponseAsNull() throws Exception {

		Mockito.when(ovenDetailsServiceImpl.getAll()).thenReturn(null);
		List<OvenDto> ovenDtoList = ovenDetailsServiceImpl.getAll();

		assertEquals(null, ovenDtoList);
	}

	@Test
	public void getOvenById_shouldReturnResponseAsNull() throws Exception {

		Mockito.when(ovenDetailsServiceImpl.getById(1L)).thenReturn(null);
		Optional<OvenDto> ovenDtoList = ovenDetailsServiceImpl.getById(1L);

		assertEquals(null, ovenDtoList);
	}

	@After
	public void tearDown() {
		oventest.clear();

	}

}
