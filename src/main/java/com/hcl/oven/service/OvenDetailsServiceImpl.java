package com.hcl.oven.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.oven.appConstant.ExceptionConstant;
import com.hcl.oven.appConstant.OvenStates;
import com.hcl.oven.entity.OvenDetails;
import com.hcl.oven.exception.IlegalOvenStateException;
import com.hcl.oven.exception.OvenDetailsNotFoundException;
import com.hcl.oven.repository.OvenDetailsRepository;
import com.hcl.oven.rest.dto.OvenDto;
import com.hcl.oven.validation.OvenDetailsValidation;

import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class OvenDetailsServiceImpl implements OvenDetailsService {

	@Autowired
	private OvenDetailsRepository ovenDetailsRepository;

	@Autowired
	private OvenDetailsValidation ovenDetailsValidation;

	/**
	 * This method will fetch oven details based on oven id
	 */
	@Override
	public Optional<OvenDto> getById(final Long id) {

		final Optional<OvenDetails> ovenDetails = ovenDetailsRepository.getOvenDetailsById(id);
		return setDataToDisplay(ovenDetails.get());
	}

	/**
	 * This method will fetch the list of oven present in database
	 */
	@Override
	public List<OvenDto> getAll() {

		final List<OvenDetails> result = this.ovenDetailsRepository.findAll();
		return setDataListToDisplay(result);
	}

	/**
	 * This method will create new oven
	 */
	@Override
	public void createOven(final List<OvenDto> ovenDtos) {

		ovenDtos.stream().forEach(dto -> {
			OvenDetails ovnDetails = new OvenDetails();
			ovnDetails.setOvenName(dto.getName());
			ovnDetails.setBrandName(dto.getBrand());
			ovnDetails.setPower(dto.getPower());
			ovnDetails.setStatus(dto.getStatus());
			ovnDetails.setTemp(dto.getTemparature());
			ovnDetails.setTimer(dto.getTime());
			ovenDetailsRepository.save(ovnDetails);

		});

	}

	/**
	 * This method will update the details about oven based on oven id.
	 */
	@Override
	public Boolean updateOven(String ovenId, final OvenDto ovenDto) {

		if (ovenDetailsValidation.checkIfOvenDetailsAlreadyPresent(ovenId)) {

			Optional<OvenDetails> ovenDetails = ovenDetailsRepository.getOvenDetailsById(Long.valueOf(ovenId));

			ovenDetails.get().setOvenName(ovenDto.getName());
			ovenDetails.get().setBrandName(ovenDto.getBrand());
			ovenDetails.get().setPower(ovenDto.getPower());
			ovenDetails.get().setStatus(null);
			ovenDetailsRepository.save(ovenDetails.get());
			return true;
		} else {
			return false;
		}

	}

	/**
	 * This method will start and stop the oven with given temperature and time
	 * based on oven id
	 * 
	 * @throws IlegalOvenStateException
	 */
	@SuppressWarnings("unlikely-arg-type")
	@Override
	public String updateOvenStatus(String temp, String time, String ovenId) throws IlegalOvenStateException {

		Optional<OvenDetails> ovenDetails = ovenDetailsRepository.getOvenDetailsById(Long.valueOf(ovenId));
		if (ovenDetails.isPresent()) {

			if (ovenDetails.get().getStatus().equals(OvenStates.POWER_ON.label)) {

				ovenDetailsRepository.updateOvenStatusAndTemp(OvenStates.BAKING.label, Integer.valueOf(temp),
						Integer.valueOf(time), Long.valueOf(ovenId));

			} else {
				if (ovenDetails.get().getStatus().equals(OvenStates.BAKING.label)) {

					return ExceptionConstant.ILLEGAL_OVEN_STATE_BAKING;
				} else if (ovenDetails.get().getStatus().equals(null)) {
					return ExceptionConstant.ILLEGAL_OVEN_STATE_BLANK;

				} else if (ovenDetails.get().getStatus().equals(OvenStates.POWER_OFF.label)) {
					return ExceptionConstant.ILLEGAL_OVEN_STATE_OFF;

				} else {
					return ExceptionConstant.ILLEGAL_OVEN_STATE;

				}
			}
		}
		return "UPDATED";

	}

	/**
	 * This method will on and off the oven based on oven id
	 */
	@SuppressWarnings("unlikely-arg-type")
	@Override
	public void updateOvenOnOffStatus(String ovenId) {

		Optional<OvenDetails> ovenDetails = ovenDetailsRepository.getOvenDetailsById(Long.valueOf(ovenId));
		if (ovenDetails.isPresent()) {

			if (ovenDetails.get().getStatus().equals(OvenStates.POWER_ON.label)
					|| ovenDetails.get().getStatus().equals(OvenStates.BAKING.label))

				ovenDetailsRepository.updateOvenStatusAndTemp(OvenStates.POWER_OFF.label, 0, 0, Long.valueOf(ovenId));
			else if (ovenDetails.get().getStatus().equals(OvenStates.POWER_OFF.label)
					|| ovenDetails.get().getStatus().equals(null) || ovenDetails.get().getStatus().equals(""))

				ovenDetailsRepository.updateOvenStatusAndTemp(OvenStates.POWER_ON.label, 0, 0, Long.valueOf(ovenId));
		}

	}

	/**
	 * This method will delete the oven detail based on given ovenId
	 */
	public void delete(String id) throws OvenDetailsNotFoundException {
		ovenDetailsRepository.deleteById(Long.valueOf(id));
	}

	private Optional<OvenDto> setDataToDisplay(final OvenDetails ovenDetail) {
		OvenDto dto = new OvenDto();
		dto.setId(ovenDetail.getId().toString());
		dto.setBrand(ovenDetail.getBrandName());
		dto.setName(ovenDetail.getOvenName());
		dto.setPower(ovenDetail.getPower());

		return Optional.of(dto);
	}

	private List<OvenDto> setDataListToDisplay(final List<OvenDetails> ovenDetail) {
		List<OvenDto> listOven = new ArrayList<OvenDto>();
		for (OvenDetails oven : ovenDetail) {

			OvenDto dto = new OvenDto();
			dto.setName(oven.getOvenName());
			dto.setId(oven.getId().toString());
			dto.setBrand(oven.getBrandName());
			dto.setPower(oven.getPower());
			dto.setStatus(oven.getStatus());
			dto.setTemparature(oven.getTemp());
			dto.setTime(oven.getTimer());

			listOven.add(dto);

		}

		return listOven;
	}

}