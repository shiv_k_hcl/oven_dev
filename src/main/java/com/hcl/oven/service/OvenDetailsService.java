package com.hcl.oven.service;

import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;

import com.hcl.oven.exception.IlegalOvenStateException;
import com.hcl.oven.exception.OvenDetailsNotFoundException;
import com.hcl.oven.rest.dto.OvenDto;

public interface OvenDetailsService {

	public List<OvenDto> getAll();

	public Optional<OvenDto> getById(Long id) throws OvenDetailsNotFoundException;

	public void createOven(List<OvenDto> dto);

	public Boolean updateOven(String id, OvenDto ovenDto);

	public String updateOvenStatus(String temp, String time, String ovenId) throws IlegalOvenStateException;

	public void updateOvenOnOffStatus(String ovenId);

	public void delete(String id) throws OvenDetailsNotFoundException;

}