package com.hcl.oven.appConstant;

public enum OvenStates {

	POWER_ON("POWER ON"), BAKING("BAKING"), POWER_OFF("POWER OFF");

	public final String label;

	private OvenStates(String label) {
		this.label = label;
	};

}
