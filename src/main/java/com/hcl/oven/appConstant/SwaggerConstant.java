package com.hcl.oven.appConstant;

public class SwaggerConstant {

	public static final String GET_OVEN_BY_ID = "View a list of available oven by id";
	public static final String GET_ALL_OVEN = "View a list of all available oven";
	public static final String ADD_OVEN = "Add a oven";
	public static final String UPDATE_OVEN = "Update oven details";
	public static final String UPDATE_OVEN_ON_OFF = "Update oven status ON/OFF";
	public static final String UPDATE_OVEN_STATUS = "Update oven status to start/stop with set temparature";
	public static final String DELETE_OVEN = "Delete oven details";

}
