package com.hcl.oven.appConstant;

public class ExceptionConstant {

	public static final String OVEN_ID_NOT_VALID = "Oven Id need to be Integer and Positive Number";
	public static final String ILLEGAL_OVEN_STATE = "Oven State is invalid cannot be changed";
	public static final String ILLEGAL_OVEN_STATE_BAKING = "Oven State is still baking cannot be changed";
	public static final String ILLEGAL_OVEN_STATE_OFF = "Oven State is powered off cannot be changed to baking";
	public static final String ILLEGAL_OVEN_STATE_BLANK = "Oven State is undefined cannot be changed to baking";

}
