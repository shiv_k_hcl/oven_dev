package com.hcl.oven.appConstant;

public class OvenConstants {

	public static final String START_OVEN = "ON";
	public static final String BAKING = "BAKING";
	public static final String STOP_OVEN = "OFF";
	public static final String TEMAPARATURE_RANGE = "TEMPARATURE RANGE SHOULD BE BETWEEN 0 TO 200 DEGREE CELSUIS";
	public static final String TIMER_RANGE = "TIMER RANGE SHOULD BE BETWEEN 0 TO 40 MINUTES";
	public static final String OVEN_UPDATED = "OVEN UPDATED SUCCESSFULLY";
	public static final String OVEN_STATUS_UPDATED = "OVEN STATUS UPDATED SUCCESSFULLY";
	public static final String OVEN_NOT_FOUND = "OVEN DID NOT EXIST";
	public static final String OVEN_ADDED = "OVEN ADDED SUCCESSFULLY";

}
