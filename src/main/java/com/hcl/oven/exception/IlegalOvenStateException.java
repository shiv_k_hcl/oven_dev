package com.hcl.oven.exception;

public class IlegalOvenStateException extends Exception {

	/**
	 * Exception thrown when state of oven is not correct.
	 */
	private static final long serialVersionUID = 1L;

	public IlegalOvenStateException(String message) {
		super(message);
	}

}
