package com.hcl.oven.exception;

public class OvenDetailsNotFoundException extends Exception {

	/**
	 * Exception thrown when no oven details are found in database
	 */
	private static final long serialVersionUID = 1L;

	public OvenDetailsNotFoundException(String message) {
		super(message);
	}

}
