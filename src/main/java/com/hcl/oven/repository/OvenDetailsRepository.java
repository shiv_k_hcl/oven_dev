package com.hcl.oven.repository;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.hcl.oven.entity.OvenDetails;
import com.hcl.oven.exception.OvenDetailsNotFoundException;

@Repository
public interface OvenDetailsRepository extends JpaRepository<OvenDetails, Long> {

	public Optional<OvenDetails> getOvenDetailsById(Long Id);

	@Modifying
	@Transactional
	@Query("update OvenDetails  set status = :status, temp= :temp,  timer= :time  where id = :id ")
	public int updateOvenStatusAndTemp(@Param("status") String status, @Param("temp") Integer temp,
			@Param("time") Integer time, @Param("id") Long id);

	@Modifying
	@Transactional
	@Query("update OvenDetails  set status = :status  where id = :id ")
	public int updateOvenOnOffStatus(@Param("status") String status, @Param("id") Long id);

}
