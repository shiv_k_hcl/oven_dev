package com.hcl.oven.validation;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hcl.oven.entity.OvenDetails;
import com.hcl.oven.repository.OvenDetailsRepository;

@Component
public class OvenDetailsValidation {

	@Autowired
	private OvenDetailsRepository ovenDetailsRepository;

	/**
	 * This method will check if given oven id is present in database or not.
	 * 
	 * @param ovenId given oven id
	 * @return
	 */
	public boolean checkIfOvenDetailsAlreadyPresent(String ovenId) {

		Optional<OvenDetails> ovenDetails = ovenDetailsRepository.findById(Long.valueOf(ovenId));

		return ovenDetails.isPresent();
	}

}
