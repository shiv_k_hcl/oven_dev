package com.hcl.oven.rest.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.oven.appConstant.ExceptionConstant;
import com.hcl.oven.appConstant.OvenConstants;
import com.hcl.oven.appConstant.SwaggerConstant;
import com.hcl.oven.exception.IlegalOvenStateException;
import com.hcl.oven.exception.OvenDetailsNotFoundException;
import com.hcl.oven.rest.dto.OvenDto;
import com.hcl.oven.service.OvenDetailsService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/ovenApp")
@Api(value = "Oven Application")
public class OvenApplicationController {

	@Autowired
	private OvenDetailsService ovenDetailsService;

	/**
	 * Get the oven details based on oven Id
	 * 
	 * @param id oven Id
	 * @return Oven request object
	 * @throws OvenDetailsNotFoundException
	 * @throws
	 */
	@ApiOperation(value = SwaggerConstant.GET_OVEN_BY_ID, response = Iterable.class)
	@GetMapping("/getOvenDetails/{id}")
	public ResponseEntity<OvenDto> getOvenDetailsById(@PathVariable final String id)
			throws OvenDetailsNotFoundException {
		try {
			Optional<OvenDto> ovenDto = ovenDetailsService.getById(Long.valueOf(id));
			if (ovenDto.isPresent()) {
				return new ResponseEntity<OvenDto>(ovenDto.get(), HttpStatus.OK);
			} else
				return new ResponseEntity<OvenDto>(HttpStatus.NOT_FOUND);
		} catch (NumberFormatException e) {

			return new ResponseEntity<OvenDto>(HttpStatus.BAD_REQUEST);

		}
	}

	/**
	 * Get the list of all oven present
	 * 
	 * @return list oven request object
	 */
	@ApiOperation(value = SwaggerConstant.GET_ALL_OVEN, response = Iterable.class)
	@GetMapping("/getAllOven")
	public ResponseEntity<Object> getAllOven() {
		List<OvenDto> list = ovenDetailsService.getAll();
		return new ResponseEntity<>(list, HttpStatus.OK);
	}

	/**
	 * Register the new oven into database
	 * 
	 * @param ovenList list for new oven
	 * @return Response Entity
	 */
	@ApiOperation(value = "register a oven")
	@PostMapping("/registerNewOven")
	public ResponseEntity<Object> registerOven(@RequestBody List<OvenDto> ovenList) {
		ovenDetailsService.createOven(ovenList);
		return new ResponseEntity<>(OvenConstants.OVEN_ADDED, HttpStatus.CREATED);

	}

	/**
	 * Update the oven details for oven Id
	 * 
	 * @param id   oven Id
	 * @param oven oven Details
	 * @return Response Entity
	 */
	@ApiOperation(value = SwaggerConstant.UPDATE_OVEN)
	@PutMapping("/updateOven/{id}")
	public ResponseEntity<Object> updateOven(@PathVariable("id") String id, @RequestBody OvenDto oven) {

		try {

			if (ovenDetailsService.updateOven(id, oven)) {

				return new ResponseEntity<>(OvenConstants.OVEN_UPDATED, HttpStatus.OK);
			}
			return new ResponseEntity<>(OvenConstants.OVEN_NOT_FOUND, HttpStatus.NOT_FOUND);
		} catch (NumberFormatException e) {

			return new ResponseEntity<>(ExceptionConstant.OVEN_ID_NOT_VALID, HttpStatus.BAD_REQUEST);

		}

	}

	/**
	 * Update the oven status baking status with temparature and timer as input.
	 * 
	 * @param temp   temparature to set
	 * @param time   timer to set
	 * @param ovenId oven Id
	 * @return Response Entity
	 * @throws IlegalOvenStateException
	 */
	@ApiOperation(value = SwaggerConstant.UPDATE_OVEN_STATUS)
	@PutMapping("/updateOvenBakingStatus/{temp}/{time}/{ovenId}")
	public ResponseEntity<Object> updateOvenBakingStatus(@PathVariable("temp") String temp,
			@PathVariable("time") String time, @PathVariable("ovenId") String ovenId) throws IlegalOvenStateException {

		if (Integer.valueOf(temp).intValue() < 0 || Integer.valueOf(temp).intValue() > 200) {
			return new ResponseEntity<>(OvenConstants.TEMAPARATURE_RANGE, HttpStatus.BAD_REQUEST);
		} else if (Integer.valueOf(time).intValue() < 0 || Integer.valueOf(time).intValue() > 40) {
			return new ResponseEntity<>(OvenConstants.TIMER_RANGE, HttpStatus.BAD_REQUEST);
		} else {
			String status = ovenDetailsService.updateOvenStatus(temp, time, ovenId);
			if (status.equals("UPDATED")) {
				return new ResponseEntity<>(OvenConstants.OVEN_STATUS_UPDATED, HttpStatus.OK);
			} else {
				throw new IlegalOvenStateException(status);
			}

		}

	}

	/**
	 * Update the oven status as ON or OFF.
	 * 
	 * 
	 * @param ovenId oven Id
	 * @return Response Entity
	 */
	@ApiOperation(value = SwaggerConstant.UPDATE_OVEN_ON_OFF)
	@PutMapping("/updateOvenOnOffStatus/{ovenId}")
	public ResponseEntity<Object> updateOvenOnOffStatus(@PathVariable("ovenId") String ovenId) {
		try {
			ovenDetailsService.updateOvenOnOffStatus(ovenId);
			return new ResponseEntity<>(OvenConstants.OVEN_STATUS_UPDATED, HttpStatus.OK);
		} catch (NumberFormatException e) {

			return new ResponseEntity<>(ExceptionConstant.OVEN_ID_NOT_VALID, HttpStatus.BAD_REQUEST);

		}

	}

	/**
	 * Delete the oven details permanently based on id
	 * 
	 * @param id oven Id
	 */
	@ApiOperation(value = SwaggerConstant.DELETE_OVEN)
	@DeleteMapping("/deleteOvenDetails/{id}")
	private void deleteOvenDetails(@PathVariable("id") String id) {
		try {
			ovenDetailsService.delete(id);
		} catch (OvenDetailsNotFoundException e) {

			e.printStackTrace();
		}
	}
}
