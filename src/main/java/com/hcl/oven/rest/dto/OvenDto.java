package com.hcl.oven.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OvenDto {

	// The oven Id
	private String id;
	// Oven name
	private String name;
	// Oven brand name
	private String brand;
	// Oven power value
	private String power;
	// Oven current status ON/BAKING/OFF
	private String status;
	// Set value of temparature
	private Integer temparature;
	// Set value of timer
	private Integer time;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getPower() {
		return power;
	}

	public void setPower(String power) {
		this.power = power;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getTemparature() {
		return temparature;
	}

	public void setTemparature(Integer temparature) {
		this.temparature = temparature;
	}

	public Integer getTime() {
		return time;
	}

	public void setTime(Integer time) {
		this.time = time;
	}
}
