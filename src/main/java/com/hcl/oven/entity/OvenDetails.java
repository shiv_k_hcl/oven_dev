package com.hcl.oven.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "T_OVEN_DETAILS")

@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class OvenDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "T_OVEN_DETAILS_SEQ")
	@SequenceGenerator(name = "T_OVEN_DETAILS_SEQ", sequenceName = "T_OVEN_DETAILS_SEQ")
	private Long id;

	@Column(name = "OVEN_NAME", nullable = false)
	private String ovenName;

	@Column(name = "BRAND_NAME")
	private String brandName;

	@Column(name = "POWER")
	private String power;

	@Column(name = "STATUS")
	private String status;

	@Column(name = "TEMPARATURE")
	private Integer temp;

	@Column(name = "TIMER")
	private Integer timer;

	@Version
	private Long version;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOvenName() {
		return ovenName;
	}

	public void setOvenName(String ovenName) {
		this.ovenName = ovenName;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getPower() {
		return power;
	}

	public void setPower(String power) {
		this.power = power;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getTemp() {
		return temp;
	}

	public void setTemp(Integer temp) {
		this.temp = temp;
	}

	public Integer getTimer() {
		return timer;
	}

	public void setTimer(Integer timer) {
		this.timer = timer;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

}
