
### OVEN APPLICATION
 


The coding assignment to write the back end services for handling oven details and status.
The services needs to be REST API with SWAGGER configured.Need to write the unit test case.
We will be using GIT hub and Bit Bucket to publish the source code.

---




### What do you need:

1.  JDK 11 ~ 'https://www.oracle.com/in/java/technologies/javase-jdk11-downloads.html'
2.  Maven 3.6 or later ~ 'https://maven.apache.org/download.cgi'
3.  Spring Tool Suite 4 or later- ' https://spring.io/tools`
4.  Install swagger.


---


###  Source Code Access:

We can get the code from below Bit Bucket repository:

`git clone https://shiv_k_hcl@bitbucket.org/shiv_k_hcl/oven_dev.git`

---

### How to Build:

mvn clean install


---

### How to Run:

mvn spring-boot:run



---


### How to execute test cases:

This project contains test cases for all the layers {controller, service & repository} of the application. 
You can open each test class under `src\test\java` and execute all of them by doing the right click(Run As).

Below is screenshot for running controller level unit test:

![Test](screenshots/ControllerTest.JPG)


---

### Oven  Services REST API description::

http://localhost:8080/swagger-ui.html#/




![Swagger](screenshots/swagger.JPG)





POST /ovenApp/addNewOven-Add a oven

**Request Payload**

[
  {
    "brand": "samsung",
    "id": "1",
    "name": "SD12",
    "power": "500W",
    "status": "string",
    "temparature": 0,
    "time": 0
  }
]


DELETE /ovenApp/deleteOvenDetails/{id} - Delete oven details

GET /ovenApp/getAllOven- View a list of all available oven

GET /ovenApp/getOvenDetails/{id} - View a list of available oven by id

PUT /ovenApp/updateOven/{id} - Update oven details

PUT /ovenApp/updateOvenStatus/{temp}/{time}/{ovenId} -Update oven status to start/stop with set temparature

Valid oven state

ON,BAKING,OFF

_____________________________________________________________________________________________________________________________________________________________________________________________________________________________

### How to operate(Rules) oven:


1) Normal Cycle

 ON-> BAKING(temparature,timer)-> OFF
 
2) When OFF, temparature and timer will automitacally will be zero.
3) When in ON stage, than only it will go to BAKING stage.
4) When oven in BAKING stage , we will be setting the temparature and timer.
 



________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________


### NOTE:

1) Due to in memory database,data will be lost once application shut down.

2) Application unit test case completed as soon as possible due to time constraint.

3) Validation is done for all the oven states.








  
